from django.urls import re_path, include
from django.urls import path
from rest_framework import routers

from .models import UserGroup
from .views import CustomUserViewSet, AllExpensesViewSet, MyExpensesViewSet, get_verification, CategoryViewSet, \
    get_group_id_by_invite_code, UserGroupViewSet

router = routers.SimpleRouter()
router.register(r'users', CustomUserViewSet)
router.register(r'all-expenses', AllExpensesViewSet)
router.register(r'my-expenses', MyExpensesViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'group', UserGroupViewSet)

urlpatterns = [
    re_path(r'^api/', include(router.urls)),
    path('bot-auth/', get_verification),
    path('group-invite/', get_group_id_by_invite_code)
]
