from django.contrib.auth.models import BaseUserManager


class CustomUserManager(BaseUserManager):
    def create_user(self, name, access_id):
        """
        Creates and saves a User with the given name and access_id.
        """
        if not name:
            raise ValueError('Users must have a name')

        user = self.model(
            name=name,
            access_id=access_id,
        )
        user.save(using=self._db)
        return user

    def create_superuser(self, name, access_id):
        """
        Creates and saves a superuser with the given name and access_id.
        """
        user = self.create_user(
            name=name,
            access_id=access_id,
        )
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user
