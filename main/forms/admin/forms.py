from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UsernameField
from django.core.exceptions import ValidationError
from main.auth_backend import CustomAuthBackend

UserModel = get_user_model()


class CustomAuthenticationForm(forms.Form):
    username = UsernameField(widget=forms.TextInput(attrs={'autofocus': True}))
    verification_code = forms.IntegerField()

    error_messages = {
        'invalid_login': "Ошибка авторизации",
        'inactive': "Пользователь был удален из системы",
    }

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        self.user_cache = None
        super().__init__(*args, **kwargs)

        self.fields['username'].max_length = self.fields['username'].widget.attrs['maxlength'] = 250

    def clean(self):
        username = self.cleaned_data.get('username')
        verification_code = self.cleaned_data.get('verification_code')

        if username is not None and verification_code:
            self.user_cache = CustomAuthBackend.authenticate(self.request,
                                                             access_id=username,
                                                             verification_code=verification_code)
            if self.user_cache is None:
                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)
        return self.cleaned_data

    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user(self):
        return self.user_cache

    def get_invalid_login_error(self):
        return ValidationError(
            self.error_messages['invalid_login'],
            code='invalid_login',
        )
