from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from .models import CustomUser, VerificationCode, UserGroup


@receiver(post_save, sender=CustomUser)
def user_post_save(instance, created, **kwargs):
    if created:
        VerificationCode.objects.create(user=instance)
        Token.objects.create(user=instance)


@receiver(pre_save, sender=CustomUser)
def user_pre_save(instance, **kwargs):
    if not instance.user_group_id:
        instance.user_group = UserGroup.objects.create()


# @receiver(post_delete, sender=CustomUser)  # TODO add logic
# def user_pre_save(instance, **kwargs):
#     pass
