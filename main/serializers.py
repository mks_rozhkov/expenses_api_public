from django.db import IntegrityError
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from .auth_backend import CustomAuthBackend
from .models import CustomUser, Expense, UserGroup, Category


class ExpenseSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.name')

    class Meta:
        model = Expense
        fields = ('id', 'name', 'money', 'created_at', 'user', 'category')

    def create(self, validated_data):
        print(f"{validated_data=}")
        try:
            return Expense.objects.create(**validated_data)
        except IntegrityError as E:
            return E


class AuthUserSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=250)
    access_id = serializers.CharField(max_length=250)
    verification_code = serializers.CharField(max_length=5)
    user_group = serializers.PrimaryKeyRelatedField(read_only=True)


class CustomUserSerializer(serializers.ModelSerializer):
    # expenses = ExpenseSerializer(many=True, read_only=True)  # , queryset=Expense.objects.all())
    user_group = serializers.PrimaryKeyRelatedField(queryset=UserGroup.objects.all(), allow_null=True)

    class Meta:
        model = CustomUser
        fields = ('name', 'access_id', 'user_group')  # 'expenses',

    def create(self, validated_data):
        print(f"{validated_data=}")
        try:
            return CustomUser.objects.create(**validated_data)
        except IntegrityError:
            return 'Произошла ошибка'


class UserGroupSerializer(serializers.ModelSerializer):
    users = CustomUserSerializer(many=True, read_only=True)  # , queryset=CustomUser.objects.all())

    class Meta:
        model = UserGroup
        fields = ('name', 'users', 'invite_code')


class CategorySerializer(serializers.ModelSerializer):
    expenses = ExpenseSerializer(many=True, required=False)

    class Meta:
        model = Category
        fields = '__all__'

    @classmethod
    def many_init(cls, *args, **kwargs):
        kwargs['child'] = cls()
        kwargs['child'].fields.pop('expenses')
        return serializers.ListSerializer(*args, **kwargs)

    def create(self, validated_data):
        print(f"{validated_data=}")
        category, created = Category.objects.update_or_create(
            name=validated_data.get('name', None),
            defaults={'user_group': validated_data.get('user_group', None)})
        return category


class CustomAuthTokenSerializer(serializers.Serializer):
    access_id = serializers.CharField(
        label=_("access_id"),
        write_only=True
    )
    verification_code = serializers.CharField(
        label=_("verification_code"),
        write_only=True
    )
    token = serializers.CharField(
        label=_("Token"),
        read_only=True
    )

    def validate(self, attrs):
        access_id = attrs.get('access_id')
        verification_code = attrs.get('verification_code')

        if access_id and verification_code:
            user = CustomAuthBackend.authenticate(self.context.get('request'),
                                                  access_id=access_id, verification_code=verification_code)

            if not user:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = _('Must include "access_id" and "verification_code".')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs
