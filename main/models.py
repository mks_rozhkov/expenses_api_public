from random import randint

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.core.validators import MaxValueValidator
from django.db import models

from .managers import CustomUserManager


def random_value(length):
    int_from = int('1' + '0' * (length - 1))
    int_to = int('9' * length)
    return str(randint(int_from, int_to))


class CustomUser(AbstractBaseUser, PermissionsMixin):
    password = None
    last_login = None
    name = models.CharField(max_length=250)
    access_id = models.CharField(max_length=250, unique=True)
    user_group = models.ForeignKey('UserGroup', blank=True,  # null=True только для user=api
                                   on_delete=models.DO_NOTHING, related_name='users')
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    objects = CustomUserManager()

    USERNAME_FIELD = 'access_id'
    REQUIRED_FIELDS = ['name']

    class Meta:
        db_table = 'custom_users'
        verbose_name = 'Пользователь приложения'
        verbose_name_plural = 'Пользователи приложения'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        models.Model.save(self, *args, **kwargs)


class VerificationCode(models.Model):
    code = models.CharField(max_length=5)
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, related_name='verification_code')
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.code = random_value(5)
        super().save(*args, **kwargs)

    class Meta:
        db_table = 'verification_codes'
        verbose_name = 'Код подтверждения'
        verbose_name_plural = 'Коды подтверждения'

    def __str__(self):
        return str(self.code)


class UserGroup(models.Model):
    name = models.CharField(max_length=250, blank=True)
    invite_code = models.CharField(max_length=10, unique=True)

    def save(self, *args, **kwargs):
        self.invite_code = random_value(10)
        super().save(*args, **kwargs)

    class Meta:
        db_table = 'user_groups'
        verbose_name = 'Группа пользователей'
        verbose_name_plural = 'Группы пользователей'

    def __str__(self):
        return str(self.name) + " - id: " + str(self.id)


class Expense(models.Model):
    name = models.CharField(max_length=250)
    money = models.PositiveIntegerField(validators=[MaxValueValidator(99999)])
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey('CustomUser', on_delete=models.CASCADE, related_name='expenses')
    category = models.ForeignKey('Category', on_delete=models.CASCADE, related_name='expenses')

    class Meta:
        db_table = 'expenses'
        verbose_name = 'Трата'
        verbose_name_plural = 'Траты'

    def __str__(self):
        return f'{self.money} - {self.name}'


class Category(models.Model):
    name = models.CharField(max_length=50)
    user_group = models.ForeignKey('UserGroup', on_delete=models.DO_NOTHING, related_name='categories')

    class Meta:
        db_table = 'categories'
        verbose_name = 'Категория трат'
        verbose_name_plural = 'Категории трат'

    def __str__(self):
        return str(self.name)
