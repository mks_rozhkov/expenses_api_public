from rest_framework import viewsets, mixins
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from main.serializers import CustomUserSerializer, ExpenseSerializer, UserGroupSerializer, AuthUserSerializer, \
    CategorySerializer
from .models import CustomUser, Expense, UserGroup, Category


class CustomUserViewSet(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer
    permission_classes_by_action = {'create': [IsAdminUser], 'get_queryset': [IsAuthenticated]}

    def get_queryset(self):
        queryset = self.queryset
        query_set = queryset.filter(user_group=self.request.user.user_group)
        return query_set

    # def create(self, request, *args, **kwargs):  # TODO можно удалить? протестировать!
    #     response = super().create(request, *args, **kwargs)
    #     return Response(response.data)

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]


@api_view(['POST'])
@permission_classes([IsAdminUser])
def get_verification(request):
    try:
        user = CustomUser.objects.get(access_id=request.data['access_id'])
        data = AuthUserSerializer(user).data
        return Response(data)
    except CustomUser.DoesNotExist:
        return Response({'error': 'Пользователь не найден'})


class AllExpensesViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Expense.objects.all()
    serializer_class = ExpenseSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = self.queryset
        query_set = queryset.filter(user__user_group=self.request.user.user_group)
        return query_set


class MyExpensesViewSet(viewsets.ModelViewSet):
    queryset = Expense.objects.all()
    serializer_class = ExpenseSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    # def create(self, request, *args, **kwargs):  # TODO удалить? В чем разница с CustomUserViewSet.create() ?
    #     # serializer = self.get_serializer(data=request.data)
    #     # self.perform_create(serializer)
    #     return super().create(request, *args, **kwargs)

    def get_queryset(self):
        queryset = self.queryset
        query_set = queryset.filter(user=self.request.user)
        return query_set


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticated]
    renderer_classes = [JSONRenderer]

    def get_queryset(self):
        queryset = self.queryset
        query_set = queryset.filter(user_group=self.request.user.user_group)
        return query_set

    # def destroy(self, request, *args, **kwargs):  # TODO delete method?
    #     result = super().destroy(request, *args, **kwargs)
    #     return result
    #
    # def retrieve(self, request, *args, **kwargs):  # TODO delete method?
    #     instance = self.get_object()
    #     serializer = self.get_serializer(instance)
    #     return Response(serializer.data)

    @action(methods=['post'], detail=False)  # TODO what is detail=False ?
    def search(self, request):
        search = request.data.get('search')
        queryset = self.queryset.filter(user_group=self.request.user.user_group,
                                        expenses__name__icontains=search).distinct()
        match = bool(queryset.filter(expenses__name=search))
        data = CategorySerializer(queryset, many=True).data
        response = {'categories': data, 'match': match}
        return Response(response)


class UserGroupViewSet(viewsets.ModelViewSet):
    queryset = UserGroup.objects.all()
    serializer_class = UserGroupSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = self.queryset
        query_set = queryset.filter(users=self.request.user)
        return query_set


@api_view(['POST'])
@permission_classes([IsAdminUser])
def get_group_id_by_invite_code(request):
    try:
        user_group = UserGroup.objects.get(invite_code=request.data['invite_code'])
        return Response(user_group.id)
    except UserGroup.DoesNotExist:
        return Response({'error': 'Инвайт-код не найден'})
