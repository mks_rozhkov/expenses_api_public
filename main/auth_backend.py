from django.contrib.auth.backends import BaseBackend
from rest_framework import parsers, renderers
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import CustomUser


class CustomAuthBackend(BaseBackend):
    def authenticate(self, access_id=None, verification_code=None, **kwargs):
        if access_id is None or verification_code is None:
            return
        try:
            user = CustomUser.objects.get(access_id=access_id)
            if user.verification_code.code == str(verification_code):
                user.verification_code.save()
                return CustomUser.objects.get(access_id=access_id)
            else:
                return None
        except CustomUser.DoesNotExist:
            return None

    def get_user(self, access_id):
        try:
            return CustomUser.objects.get(pk=access_id)
        except CustomUser.DoesNotExist:
            return None


class ObtainAuthToken(APIView):
    from .serializers import CustomAuthTokenSerializer
    throttle_classes = ()
    permission_classes = (IsAdminUser,)
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = CustomAuthTokenSerializer

    def get_serializer_context(self):
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self
        }

    def get_serializer(self, *args, **kwargs):
        kwargs['context'] = self.get_serializer_context()
        return self.serializer_class(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key})


obtain_auth_token = ObtainAuthToken.as_view()
