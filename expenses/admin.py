from django.contrib import admin

from main.forms.admin.forms import CustomAuthenticationForm
from main.models import CustomUser, VerificationCode, UserGroup, Expense, Category


class CustomAdminSite(admin.AdminSite):
    site_header = 'Expenses App admin panel'
    login_template = 'admin/login.html'
    login_form = CustomAuthenticationForm


admin_site = CustomAdminSite(name='custom_admin')

admin_site.register([CustomUser, VerificationCode, UserGroup, Expense, Category])
